﻿using KPZExamBackend.Data;
using KPZExamBackend.Data.Models;
using Microsoft.AspNetCore.Mvc;

namespace KPZExamBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HistoryController : ControllerBase
    {
        private InventoryDbContext _inventoryDbContext;
        public HistoryController(InventoryDbContext inventoryDbContext) 
        {
            _inventoryDbContext= inventoryDbContext;
        }

        [HttpGet]
        public IEnumerable<HistoryRecord> Get()
        {
            return _inventoryDbContext.HistoryRecords.ToList();
        }

        [HttpPost]
        public void AddRecord(HistoryRecord record) 
        {
            _inventoryDbContext.HistoryRecords.Add(record);
            _inventoryDbContext.SaveChanges();
        }

        [HttpPatch]
        public void EditRecord(HistoryRecord record)
        {
            _inventoryDbContext.HistoryRecords.Update(record);
            _inventoryDbContext.SaveChanges();
        }

        //todo add validation
        [HttpDelete("{id}")]
        public void DeleteRecord(int id)
        {
            var record = _inventoryDbContext.HistoryRecords.FirstOrDefault(r => r.Id == id);
            _inventoryDbContext.HistoryRecords.Entry(record).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
            _inventoryDbContext.SaveChanges();
        }
    }
}
