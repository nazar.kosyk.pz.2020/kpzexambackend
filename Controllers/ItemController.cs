﻿using KPZExamBackend.Data;
using KPZExamBackend.Data.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace KPZExamBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ItemController : ControllerBase
    {
        private InventoryDbContext _inventoryDbContext;
        public ItemController(InventoryDbContext inventoryDbContext)
        {
            _inventoryDbContext = inventoryDbContext;
        }

        [HttpGet]
        public IEnumerable<Item> Get()
        {
            return _inventoryDbContext.Items.ToList();
        }

        [HttpPost]
        public void AddItem(Item item) 
        {
            _inventoryDbContext.Items.Add(item);
            _inventoryDbContext.SaveChanges();
        }

        [HttpPatch]
        public void EditItem(Item item) 
        {
            _inventoryDbContext.Items.Update(item);
            _inventoryDbContext.SaveChanges();
        }

        //todo add validation
        [HttpDelete("{id}")]
        public void DeleteItem(int id) 
        {
            var item = _inventoryDbContext.Items.FirstOrDefault(i => i.Id == id);
            _inventoryDbContext.Items.Entry(item).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
            _inventoryDbContext.SaveChanges();
        }

    }
}
