﻿using System.ComponentModel.DataAnnotations;

namespace KPZExamBackend.Data.Models
{
    public class HistoryRecord
    {
        [Key]
        public int Id { get; set; }
        public string? SurnameOfUser { get; set; }
        public int UsedItemId { get; set; }
        public int YearOfStart { get; set; }
        public int YearOfEnd { get; set; }
    }
}
