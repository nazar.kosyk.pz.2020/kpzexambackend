﻿using System.ComponentModel.DataAnnotations;

namespace KPZExamBackend.Data.Models
{
    public class Item
    {
        [Key]
        public int Id { get; set; }
        public string? Name { get; set; }
        public double? Price { get; set; }  
        public int? YearOfRelease { get; set; }
        public int? RoomId { get; set; }
        public string? DateOfWriteOff { get; set; }
    }
}
