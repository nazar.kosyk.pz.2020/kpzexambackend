﻿using KPZExamBackend.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace KPZExamBackend.Data
{
    public class InventoryDbContext : DbContext
    {
        public InventoryDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<HistoryRecord> HistoryRecords { get; set; }
        public DbSet<Item> Items { get; set; }
    }
}
